public class Main {

    public static void main(String[] args)
    {
        int dni = 31+29+31;
        System.out.println("Zadanie 1: "+ dni);

        int suma=1+2+3+4+5+6+7+8+9+10;
        System.out.println("Zadanie 2: " + suma);

        int iloczyn=1*2*3*4*5*6*7*8*9*10;
        System.out.println("Zadanie 3: " + iloczyn);

        System.out.println("Zadanie 4: ");
        double kasa;
        kasa = 1000;
        double rok1=kasa*1.06;
        double rok2=rok1*1.06;
        double rok3=rok2*1.06;
        System.out.println("Po pierwszym roku: " + rok1);
        System.out.println("Po drugim roku: " + rok2);
        System.out.println("Po trzecim roku: " + rok3);


        System.out.println("Zadanie 5: ");
        System.out.println("+____________+");
        System.out.println("|            |");
        System.out.println("|    Java    |");
        System.out.println("|            |");
        System.out.println("+ ___________+");


        System.out.println("Zadanie 6: ");
        System.out.println("   ////     ");
        System.out.println(" +'''''''+ ");
        System.out.println("(|  o  o |)  ");
        System.out.println(" |   P   |   ");
        System.out.println(" |  '^^' |   ");
        System.out.println(" +-------+  ");

        System.out.println("Zadanie 7: ");
        System.out.println(" *        *    *      *   *******  *******  ******  *******  *    *   *******  ********         *         **************  *******                 ");
        System.out.println(" *        *    *      *   *     *  *        *    *     *     *   *    *     *         *        * *        *                     *   ");
        System.out.println(" *        *    *      *   *     *  *        *    *     *     *  *     *     *        *        *   *       *                    *             ");
        System.out.println(" *        *    *      *   *     *  *        *    *     *     * *      *     *       *        *     *      *                   *   ");
        System.out.println(" **********    *      *   *******  *******  *****      *     **       *     *      *        *       *     *                  *   ");
        System.out.println(" *        *    *      *   *     *  *        * *        *     **       *     *     *        ***********    *                 *                      ");
        System.out.println(" *        *    *      *   *     *  *        *  *       *     * *      *     *    *        *           *   *                *                   ");
        System.out.println(" *        *    *      *   *     *  *        *   *      *     *  *     *     *   *        *             *  *               *                 ");
        System.out.println(" *        *    ********   *******  *******  *    *     *     *   *    *******  *******  *               * ************** *********                                     ");

        System.out.println("Zadanie 8: ");
        System.out.println("    +      ");
        System.out.println("   + +     ");
        System.out.println("  +   +    ");
        System.out.println(" +-----+   ");
        System.out.println(" | .-. |   ");
        System.out.println(" | | | |   ");
        System.out.println(" +-+-+-+   ");

        System.out.println("Zadanie 9: ");
        System.out.println("   _q_p_     ------    ");
        System.out.println("  ( ^ ^ )  | Hello |  ");
        System.out.println("  (  u  ) <  Junior|  ");
        System.out.println("   | | |   | Coder |  ");
        System.out.println("  (__|__)   ------    ");

        System.out.println("Zadanie 10: ");
        System.out.println("Drive \nOnly god forgives \nAlien \n");

        System.out.println("Zadanie 11:Buson (1775)\n");
        System.out.println("UIkk no \n" +
                "gan ya hayama ni \n" +
                "tsuki o in su  \n" );

        System.out.println("Zadanie 12: ");
        System.out.println("********==================");
        System.out.println("********==================");
        System.out.println("********==================");
        System.out.println("==========================");
        System.out.println("==========================");
        System.out.println("==========================");
    }
}